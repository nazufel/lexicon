// lexicon/cmd/lexicon-server/main.go

// main glues everything together and bootstraps the app

package main

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/ryanthebossross/lexicon/pkg/health"
	"gitlab.com/ryanthebossross/lexicon/pkg/http/rest"
	"gitlab.com/ryanthebossross/lexicon/pkg/sagas"
	"gitlab.com/ryanthebossross/lexicon/pkg/storage/mongo"
	"go.uber.org/zap"
)

// main bootstaps the rest of the app
func main() {

	var lg *zap.Logger

	// TODO: figure out how to change the timestamp format
	lg, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("unable to use the zap logger: %v", err)
	}

	if os.Getenv("logLevel") == "debug" {
		// TODO: figure out how to change the timestamp format
		lg, err = zap.NewDevelopment()
		if err != nil {
			log.Fatalf("unable to use the zap logger: %v", err)
		}
	}

	defer lg.Sync()

	lg.Info("using zap for logging,", zap.String("logLevel", os.Getenv("logLevel")))

	sp := os.Getenv("servicePortENV")

	if sp == "" {
		lg.Fatal("servicePortENV env variable is required and not defined", zap.String("servicePorENV", sp))
	}

	var h health.Service
	var sg sagas.Service

	s, err := mongo.NewStorage(lg)
	if err != nil {
		lg.Fatal("setting up new storage", zap.Error(err))
	}

	h = health.NewService(s)
	sg = sagas.NewService(s)

	// init router
	r := rest.Router(h, sg, lg)

	lg.Info("webserver is up and running", zap.String("port", sp))
	// start the webserver and listen on a port and use the routes package
	err = http.ListenAndServe(":"+sp, r)
	if err != nil {
		lg.Fatal("failed to start up webserver", zap.Error(err))
	}
}
