// lexicon/cmd/seedData/main.go

// program to seed the local db with development data

package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/ryanthebossross/lexicon/pkg/sagas"
	"gitlab.com/ryanthebossross/lexicon/pkg/storage/mongo"
	"go.uber.org/zap"
)

// main bootstaps the seed DB
func main() {

	lg, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("unable to use the zap logger: %v", err)
	}
	defer lg.Sync()

	lg.Info("using zap for logging.")

	// init the Service interface
	var sg sagas.Service

	// init the storage interface and check the error
	s, err := mongo.NewStorage(lg)
	if err != nil {
		lg.Fatal("unable to establish new storage", zap.Error(err))
	}

	sg = sagas.NewService(s)

	seedSagaData(sg, s, lg)
}

// seedSagaData func seeds the database of Config data
func seedSagaData(sg sagas.Service, r sagas.Repository, lg *zap.Logger) error {

	col := "sagas"

	// drop collection drops the configitems collection if env bool is defiened, and returns an error.
	err := r.DropSagasCollection(col, lg)
	if err != nil {
		lg.Fatal("unable to drop "+col+" collection", zap.Error(err))
	}

	// define file location relative path
	seedPath := "./seedSagasData.json"

	// check for the seed file
	_, err = os.Stat(seedPath)
	if err != nil {
		lg.Fatal("seed file not found", zap.String("seedPath", seedPath), zap.Error(err))
	}

	// print out file info
	lg.Info("found seed file", zap.String("path", seedPath))

	file, err := ioutil.ReadFile(seedPath)
	if err != nil {
		lg.Fatal("unable to read file", zap.String("seedPath", seedPath), zap.Error(err))
	}

	var sgs []sagas.Saga

	err = json.Unmarshal([]byte(file), &sgs)
	if err != nil {
		lg.Fatal("unable to marshal seed data object into []saga", zap.String("seedPath", seedPath), zap.Error(err))
	}

	for i := 0; i < len(sgs); i++ {
		err = sg.CreateSaga(sgs[i], lg)
		if err != nil {
			break
		}
	}

	// log seeding the db is done
	lg.Info("inserted seed documents", zap.Int("number", len(sgs)))

	return nil
}
