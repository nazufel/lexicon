# lexicon

Lexicon is a cloud-native web service for creating, storing, securing, and accessing complex configuration items for software systems. Lexicon can manage configuration items to be accessed by CI/CD tools, technical and non-technical teams, and software systems.

Lexicon is a working name and unlikely to be the final project name. 

## Why Lexicon

Lexicon has a very flexiable REST api. You only need these four things to get your configuration item:
* **Application Name** - The identity of the requesting application
* **Scope** - Config items can have scopes as defined by the administrator. Configs can be globally bound, or limited to smaller scopes such as a cloud provider region or VPC, or as granular as individual Kubernetes clusters. 
* **Environment** - The SDLC environment the requesting code is currently in: Dev, E2E, Staging, Canary, Prod, etc.
* **Config Name** - The name of the configuration item being requested. Names must be unique.

## Getting Started

Getting Lexicon up and running is easy. Follow the [installation](./docs/installation.md) documentation.

## Examples

Check out the examples [documentation](https://gitlab.com/ryanthebossross/lexicon/tree/master/docs/examples) for how to use your running server.

## Types of API Objects

* Saga
* User

## Dependencies
Depenencies are inevitable, despite the Golang comunity's emphasis on the standard library. Lexicon takes dependencies very seriously. The project strives for if third-party dependencies are necessary, then those dependences will be vetted along the following criteria:
* Is the official library of another project
* Has no external dependencies of it's own other than the standard library
* Includes tests

### Packages
Lexicon was designed to use a minimum number external dependencies. An idiom of Golang is to use the Standard Library first. Lexicon attempts the same. Here's the list of extenral dependencies:

* [httprouter](https://github.com/julienschmidt/httprouter) is an HTTP router by Julien Schmidt 
* [mongodb-go-driver](https://github.com/mongodb/mongo-go-driver) the ORM for MongoDB by MongoDB
* [uber-go/zap](https://github.com/uber-go/zap) is a structured logging library by Uber

I'm trying to minimize the use of non-standard library packages. I'm using the httprouter because it's the router I learned to use when I first started writing Go. The package is small and widely used. If there is ever an issue with support or deprication, Lexicon uses Clean Architecture. The http router is just an implimentation detail. A new router, or the default Golang router can be easily swapped out. 

The MongodB driver is unavoidable. It's the official MongoDB driver for Go.

### Database

Lexicon uses [MongoDB 4.2](https://www.mongodb.com/download-center) for a backend.

## Architecture

## Version 0.1 MVP
* Data Encryption at Rest - Done
* CRUD of Config objects - Done
* CRUD of User objects
* Stable ReSTful API - In Progress
* Stable Config Data Model In Progress
* Stable User Data Model
* Stable get config with name, environment, region/host, and application
* 90% code coverage with tests
* Automated testing: unit, integration, and regression
* User management: Local and oAuth2
* Helm Chart Packaging
* Stable and latest Docker builds
* Docker-compose and Kubernetes manifest files for development - Done
* Multi-tenancy - In Progress
* Proper logging to follow standard best practices - In Progress

## MAINTAINER

Ryan Ross - ryanthebossross@gmail.com