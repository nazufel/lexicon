#!/bin/zsh

# devEnvVars.sh

# MAINTAINER: Ryan Ross - ryanthebossross@gmail.com

# Usage: Running this script with the typical "./devEnvVars.sh" won't export variables to executing shell. Shell scripts have their own environment when executed. 
#+Run the script as ". devEnvVars.sh" to export to non-script environment

# script to export local env variables required for running lexicon. Don't use this variables in outside of local development!

unset dbHostENV
unset dbNameENV
unset dbPasswordENV
unset dbPortENV
unset dbUsernameENV
unset dropColENV
unset logLevel
unset servicePortENV
unset sagasCollectionENV

export dbHostENV=localhost
export dbNameENV=dev-lexikon
export dbPasswordENV=root
export dbPortENV=8081
export dbUsernameENV=root
export dropColENV=true
export GO111MODULE=on
export KUBERNETES_SERVICE_HOST=localhost
export KUBERNETES_SERVICE_PORT=443
# export logLevel=debug
export sagasCollectionENV=sagas
export servicePortENV=3000

printf "Finished exporting local development variables. Happy coding!\n\n"
