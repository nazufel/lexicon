# Business Rules and Usecases

1. All Configurations must have `Scope` defined. Acceptable values are: `Connection String`, `Password`, `Hostname`, `DNS Name`, or `Key Value` - Completed: 8/20/19, [Commit: a31b1140efcfcbdaf9f70ba13c6d1620b56f0470](https://gitlab.com/ryanthebossross/lexicon/commit/a31b1140efcfcbdaf9f70ba13c6d1620b56f0470)
2. Users can only Read Configurations that grant Read permissions to a group the user is a member of
3. Users can only Write Configurations that grant Write permissions to a group the user is a member of
4. Users can only Delete Configurations that grant Delete permissions to a group the user is a member of
5. Deleted Configurations and Users are retained for 30 days (default) and can be restored. This default value can also be changed, but only by a Company owner.
6. Configuration values must be unique. If a collision occurs, respond with best guesses of similar configurations. This can be overriden by a Type administrator.
