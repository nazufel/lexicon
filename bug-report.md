# Bug Report

## Reports

### 

Bug001 - 27/08/2019: 

#### Actual Behavior
Running a GET request on /:company/configs to return all the configs will return all configs, but some configs have an extra information in the response from Get All. Connection-string has the correct data. Password types have connection-string and password data. Hostnames types have connection-string, password, and hostnames. Finally, key-value types have all data types. 

```json
{
    "name": "name-of-the-foo-server",
    "type": "hostnames",
    "data": {
        "connection-string": "redis00:6380,redis01:6380,redis02,allowAdmin=false",
        "username": "lexicon-app-db-user-prod",
        "password": "thi9sise93ndn*gpaasdecRd",
        "hostnames": [
            "foo1.foo.com"
        ],
        "key": "Auth0_ClientID",
        "value": "a6sdf4a6s5df4a6s5df46a5sf989e8f"
    }
}
```
Now, if running a GET on a specific config by name, such as /:company/configs/name-of-the-foo-server it returns the expected list of hostnames:

```json
{
    "hostnames": [
        "foo1.foo.com"
    ]
}
```
 When describing `?describe=true` the returned data is correct:

```json
{
    "name": "name-of-the-foo-server",
    "type": "hostnames",
    "data": {
        "hostnames": [
            "foo1.foo.com"
        ],
    }
}
```
Looking at the DB, all of the configs are correct.

It appears the problem is with the GET All path. Since the data struct just holds strings, the values are bleeding over from one Config into the next one to be returned.

#### Expected Behavior
Running a GET request on /:company/configs to return all the configs with the proper data values without bleed over.



#### How to Reproduce
Running a GET request on /:company/configs to return all the configs. Check to see the `"data"` field to see if the values match the DB or what are expected. A config of type `password` should not return:

```json
{
    "name": "name-of-the-foo-server",
    "type": "hostnames",
    "data": {
        "connection-string": "redis00:6380,redis01:6380,redis02,allowAdmin=false",
        "username": "lexicon-app-db-user-prod",
        "password": "thi9sise93ndn*gpaasdecRd",
        "hostnames": [
            "foo1.foo.com"
        ],
        "key": "Auth0_ClientID",
        "value": "a6sdf4a6s5df4a6s5df46a5sf989e8f"
    }
}
```

I expect it to return:

```json
{
    "name": "name-of-the-foo-server",
    "type": "hostnames",
    "data": {
        "hostnames": [
            "foo1.foo.com"
        ],
    }
}
```

