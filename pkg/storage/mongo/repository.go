package mongo

import (
	"context"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
)

var (
	dbName            string
	configsCollection string
	sagasCollection   string
	dbConnection      string
	dbHost            string
	dbUsername        string
	dbPassword        string
	dbPort            string
	dropColENV        string
	livenessENV       string
	ctx               context.Context
)

// Storage struct holds information about connecting to the DB
type Storage struct {
	db *mongo.Database
	c  *mongo.Client
}

// NewStorage connects to the DB on start up
func NewStorage(lg *zap.Logger) (*Storage, error) {

	var err error

	if len(os.Getenv("dbNameENV")) == 0 {
		lg.Fatal("dbNameENV is a required env variable. Please define a databae to use.")
	}
	dbName = os.Getenv("dbNameENV")

	if len(os.Getenv("sagasCollectionENV")) == 0 {
		lg.Fatal("sagasCollectionENV is a required env variable. Please define a collection for sagas.")
	}
	sagasCollection = os.Getenv("sagasCollectionENV")

	if len(os.Getenv("dbHostENV")) == 0 {
		lg.Fatal("dbHostENV is a required env variable. Please define a database host string to use.")
	}
	dbHost = os.Getenv("dbHostENV")

	if len(os.Getenv("dbUsernameENV")) == 0 {
		lg.Fatal("dbUsernameENV is a required env variable. Please define a database username string to use.")
	}
	dbUsername = os.Getenv("dbUsernameENV")

	if len(os.Getenv("dbPasswordENV")) == 0 {
		lg.Fatal("dbPasswordENV is a required env variable. Please define a database password string to use.")
	}
	dbPassword = os.Getenv("dbPasswordENV")

	if len(os.Getenv("dbPortENV")) == 0 {
		lg.Fatal("dbPortENV is a required env variable. Please define a database port string to use.")
	}
	dbPort = os.Getenv("dbPortENV")

	s := new(Storage)

	dbConnection = "mongodb://" + dbUsername + ":" + dbPassword + "@" + dbHost + ":" + dbPort + "/" + dbName + "?authSource=admin"

	client, err := mongo.NewClient(options.Client().ApplyURI(dbConnection))
	if err != nil {
		lg.Fatal("unable to create database client", zap.String("dbConnection", dbConnection))
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	lg.Info("attempting to establish database connection", zap.String("dbConnection", dbConnection), zap.Int("timeoutSeconds", 10))

	err = client.Connect(ctx)
	if err != nil {
		lg.Fatal("unable to connect to database", zap.String("dbConnection", dbConnection), zap.Int("timeoutSeconds", 10), zap.Error(err))
	}
	s.db = client.Database(dbName)
	s.c = client

	lg.Info("database connection successfully established", zap.String("dbConnection", dbConnection))

	return s, nil
}
