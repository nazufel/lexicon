package mongo

import (
	"go.uber.org/zap"
)

// PollDB handler is a readiness probe for kubernetes to check if the app has a connection to the DB.
func (s *Storage) PollDB(lg *zap.Logger) error {
	err := s.c.Ping(ctx, nil)
	if err != nil {
		lg.Error("unable to liveness poll database", zap.Error(err))
		return err
	}
	return nil
}
