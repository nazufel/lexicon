//lexicon/pkg/storage/mongo/sagas.go

// this file holds DB operations for the Sagas package

package mongo

import (
	"context"
	"errors"
	"os"
	"time"

	"gitlab.com/ryanthebossross/lexicon/pkg/sagas"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
)

/////////////////////////////////
// CRUD Operations for Sagas //
/////////////////////////////////

// CREATE //

// CreateSaga inserts one item from the Saga model
func (s *Storage) CreateSaga(sg sagas.Saga, lg *zap.Logger) error {

	// set the time of creation and updated for the Saga
	sg.Metadata.Created = time.Now().UTC()
	sg.Metadata.Updated = time.Now().UTC()

	// insert Saga and check for the error
	_, err := s.db.Collection(sagasCollection).InsertOne(context.Background(), sg)
	if err != nil {
		lg.Error("unable to insert document into the database", zap.String("name", sg.Name), zap.Error(err))
		return err
	}
	lg.Info("inserted document into the database", zap.String("name", sg.Name))
	return nil
}

// READ //

// GetSaga takes the saga name, searches the DB, and returns the saga
func (s *Storage) GetSaga(n string, lg *zap.Logger) (sagas.Saga, error) {

	var sg sagas.Saga

	// filter by usernames
	filter := bson.D{bson.E{Key: "name", Value: n}}

	// search the db by the filter and check for error
	err := s.db.Collection(sagasCollection).FindOne(context.Background(), filter).Decode(&sg)
	if err != nil {
		lg.Info("requested saga not found in database", zap.String("name", n), zap.Error(err))
		return sg, sagas.ErrNotFound
	}

	lg.Debug("successfully found saga in database", zap.String("name", sg.Name))
	// return saga object
	return sg, nil
}

// GetSagas takes a company string and an empty interface and searches the db based on filters and returns the sagas and an error
func (s *Storage) GetSagas(q map[string][]string, lg *zap.Logger) ([]sagas.Saga, error) {

	// set find options behavior
	findOptions := options.Find()
	// findOptions.SetLimit(25)

	// filter by company, this is the default behavior
	filter := bson.D{{}}

	// loop over the query params map to get the key:value pairs
	for k, v := range q {
		for i := 0; i < len(v); i++ {
			if v[i] != "" {
				pe := bson.E{Key: k, Value: v[i]}

				// append the filter with the latest primative
				filter = append(filter, pe)

				// clear out value of the primative
				pe = bson.E{}
			}
		}
	}

	// get all sagas from the db and check for error
	cur, err := s.db.Collection(sagasCollection).Find(context.Background(), filter, findOptions)
	if err != nil {
		lg.Error("error retrieving sagas from database", zap.Error(err))
	}
	defer cur.Close(context.Background())

	// init slice of model
	var sgs []sagas.Saga

	// Loop over the cursor from the db, check for an error, and append the error free object the sagas object
	for cur.Next(context.Background()) {

		// init single model object everytime the loop runs
		var sg sagas.Saga

		// decode curser into saga struct
		err := cur.Decode(&sg)
		if err != nil {
			lg.Error("unable to decode sagas from database", zap.Error(err))
		}
		sgs = append(sgs, sg)
	}
	// check the error
	if err := cur.Err(); err != nil {
		lg.Error("error with client cursor", zap.Error(err))
	}

	if len(sgs) == 0 {
		lg.Info("serch query returned 0 documents")
	}

	if len(sgs) == 1 {
		lg.Info("found 1 document in the database", zap.String("name", sgs[0].Name))
	}
	if len(sgs) > 1 {
		lg.Info("found multiple documents in the database", zap.Int("number", len(sgs)))
		for i := 0; i < len(sgs); i++ {
			lg.Info("found document in the database", zap.String("name", sgs[i].Name))
		}
	}

	// return sagas list object
	return sgs, nil
}

// UPDATE //

//TODO: update the fields

// UpdateSaga updates a Saga
func (s *Storage) UpdateSaga(n string, sg sagas.Saga, lg *zap.Logger) error {

	filter := bson.D{primitive.E{Key: "name", Value: n}}

	update := bson.M{"$set": bson.M{
		"name":                 sg.Name,
		"application":          sg.Application,
		"metadata.description": sg.Metadata.Description,
		"metadata.author":      sg.Metadata.Author,
		"metadata.owner":       sg.Metadata.Owner,
		"metadata.tags":        sg.Metadata.Tags,
		"metadata.updated":     time.Now(),
		"company":              sg.Company,
		"scopes":               sg.Scopes,
		"permissions":          sg.Permissions,
	},
	}

	_, err := s.db.Collection(sagasCollection).UpdateOne(context.Background(), filter, update)
	if err != nil {
		lg.Error("error updating saga", zap.String("name", sg.Name))
		return err

	}

	lg.Info("saga saved to database", zap.String("name", sg.Name))
	return nil
}

// DELETE //

// DeleteSaga deletes a Saga
func (s *Storage) DeleteSaga(n string, lg *zap.Logger) error {

	// filter by usernames
	filter := bson.D{primitive.E{Key: "name", Value: n}}

	// search the db by the filter and check for error
	_, err := s.db.Collection(sagasCollection).DeleteOne(context.Background(), filter)
	if err != nil {
		return sagas.ErrNotFound
	}

	lg.Debug("deleted saga", zap.String("name", n))

	// return nil error
	return nil
}

// DropSagasCollection drops the collection for seedDB purposes
func (s *Storage) DropSagasCollection(col string, lg *zap.Logger) error {

	drop := os.Getenv("dropColENV")

	if drop != "true" {
		lg.Info("dropColENV is not set to true", zap.String("dropColENV", drop))
		DropToggle := errors.New("unable to drop " + col + " collection becuase dropColENV is set to true")
		return DropToggle
	}
	err := s.db.Collection(sagasCollection).Drop(context.Background())
	if err != nil {
		lg.Fatal("unable drop the specified collection", zap.String("name", col), zap.Error(err))
	}
	lg.Info("specified collection dropped", zap.String("name", col))

	return nil
}
