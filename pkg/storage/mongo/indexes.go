// lexicon/pkg/storage/mongo/indexes.go

// this file contains the functions to creating indexes

package mongo

/*
commenting out to work on later.

Basically, i want to do this from the Mongo Docs:

db.collection.createIndex(
   {
     content: "text",
     keywords: "text",
     about: "text"
   },
   {
     weights: {
       content: 10,
       keywords: 5
     },
     name: "TextIndex"
   }
 )

*/

// // populateIndex func populates the index
// func populateIndex(database, collection string, client *mongo.Client) {
// 	c := client.Database(dbName).Collection(collection)
// 	opts := options.CreateIndexes().SetMaxTime(10 * time.Second)
// 	index := yieldIndexModel()
// 	c.Indexes().CreateOne(context.Background(), index, opts)
// 	log.Println("Successfully created the index")
// }

// // yieldIndexModel does a thing
// func yieldIndexModel() mongo.IndexModel {
// 	keys := bsonx.Doc{{Key: "name", Value: bsonx.Int32(int32(1))}}
// 	index := mongo.IndexModel{}
// 	index.Keys = keys
// 	index.Options = bsonx.Doc{{Weights: {1}}}
// 	return index
// }

// // listIndexes does a thing
// func listIndexes(client *mongo.Client, database, collection string) {
// 	c := client.Database(database).Collection(collection)
// 	duration := 10 * time.Second
// 	batchSize := int32(10)
// 	cur, err := c.Indexes().List(context.Background(), &options.ListIndexesOptions{&batchSize, &duration})
// 	if err != nil {
// 		log.Fatalf("Something went wrong listing %v", err)
// 	}
// 	for cur.Next(context.Background()) {
// 		index := bson.D{}
// 		cur.Decode(&index)
// 		log.Println(fmt.Sprintf("index found %v", index))
// 	}
// }
