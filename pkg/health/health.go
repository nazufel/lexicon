package health

import "go.uber.org/zap"

// liveness handler responds with 200 header if server is running
func (s *service) Liveness(lg *zap.Logger) error {
	err := s.r.PollDB(lg)
	return err
}
