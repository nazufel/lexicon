package health

import "go.uber.org/zap"

// Service interface makes contained confings methods available outside of the package
type Service interface {
	Liveness(*zap.Logger) error
}

// Repository interface makes contained methods of the Repository package(s) available to this package
type Repository interface {
	PollDB(*zap.Logger) error
}

// struct holds references to other package Service interfaces and makes them available to this package
type service struct {
	r Repository
}

// NewService takes the repository interface and returns a pointer to the repository service
func NewService(r Repository) Service {
	return &service{r}
}
