package rest

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/ryanthebossross/lexicon/pkg/health"
	"gitlab.com/ryanthebossross/lexicon/pkg/sagas"
	"go.uber.org/zap"
)

// Router func does all of the routing for the handlers
func Router(h health.Service, sg sagas.Service, lg *zap.Logger) http.Handler {
	r := httprouter.New()

	///////////////
	// CRUD Saga //
	///////////////

	// CREATE //

	// POST requestion on /configs calls the addConfig func and it takes the methods available to the config service
	r.POST("/api/rest/saga", createSaga(sg, lg))

	// READ //

	// GET request calls the getAllSagas func and it takes the sagas service
	r.GET("/api/rest/saga", getSagas(sg, lg))

	// GET request on /configs calls the getAllConfigs func and it takes the configs service
	r.GET("/api/rest/saga/:sagaName", getSaga(sg, lg))

	// UPDATE //

	// PUT request on /configs/:name calls the updateConfig func and it takes the configs service
	r.PUT("/api/rest/saga/:sagaName", updateSaga(sg, lg))

	// DELETE //

	// DELETE request on /configs/:name calls the deleteConfig func and it takes the configs service
	r.DELETE("/api/rest/saga/:sagaName", deleteSaga(sg, lg))

	////////////
	// HEALTH //
	////////////

	// GET request on /metrics/livenessprobe
	r.GET("/metrics/livenessprobe", healthz(h, lg))

	// return the router object to be used by main
	return r
}
