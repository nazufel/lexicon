package rest

// Trees holds all of the http handling for the Trees package.

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/ryanthebossross/lexicon/pkg/sagas"
	"go.uber.org/zap"
)

// createSaga func takes the adding service and http stuff and returns a
func createSaga(sg sagas.Service, lg *zap.Logger) func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

		// listen to the json request and decode the body
		decoder := json.NewDecoder(r.Body)

		// init Sagas model struct
		var s sagas.Saga

		// decode the request body into the struct and check the erorr
		err := decoder.Decode(&s)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// call the Create method in the Trees package to add the Tree and check for validation errors
		err = sg.CreateSaga(s, lg)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		lg.Info("redirecting to newly created saga", zap.String("url", "http://"+r.Host+r.RequestURI+"/"+s.Name))
		http.Redirect(w, r, "http://"+r.Host+r.RequestURI+"/"+s.Name, http.StatusFound)
		return
	}
}

// getSagas func takes an http request, parses the route and query params, and passes them to the Sagas package to return all Sagas based on passed params
func getSagas(sg sagas.Service, lg *zap.Logger) func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// return http stuff, and a list of all Trees based on route params
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

		// get request url query as a map[string][]string
		qp := r.URL.Query()

		// pass company name and query values to Sagas package, check for returned Sagas and the error
		// not logging the error here because it originates in sg.GetSagas. it's logged there
		list, err := sg.GetSagas(qp, lg)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if len(list) == 0 {
			http.Error(w, "Could not find a Saga based on query parameters.", http.StatusNotFound)
			return
		}

		if len(list) == 1 {
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(list[0])
		}

		// set repsonse headers and write the list of Trees to response writer
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(list)
	}
}

type updatedResponse struct {
	Name    sagas.Saga `json:"name"`
	Updated sagas.Saga `json:"updated"`
}

// getSaga parses the router params and passes the string to the package to search for Saga by name
func getSaga(sg sagas.Service, lg *zap.Logger) func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

		// get the saga name from router params
		n := p.ByName("sagaName")

		// pass saga name to GetSaga and save the return
		saga, err := sg.GetSaga(n, lg)
		// check the error
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}

		// parse url query params
		qp := r.URL.Query()

		// search for scope
		scope := qp.Get("scope")

		// check for environment in query params
		env := qp.Get("environment")

		// if env is defined and scope is not, then return an error
		if env != "" && scope == "" {
			http.Error(w, "please define a scope with environment: "+env+" in requested saga: "+saga.Name, http.StatusNotFound)
			return
		}
		// if scope is not empty
		if scope != "" {
			var scopeIndex int
			var foundScope bool
			lg.Info("scope requested", zap.String("scope", scope))
			lg.Info("returning number of environments", zap.Int("environments", len(scope)))
			// loop over the saga scopes looking for one that matches what was asked for in the query params
			for s := range saga.Scopes {
				if saga.Scopes[s].Name == scope {
					scopeIndex = s
					foundScope = true
					break
				}
			}
			// if scope wasn't found then return an error
			if !foundScope {
				lg.Info("scope requested was not found", zap.String("environment", scope))
				http.Error(w, "requested scope: "+scope+" was not found in saga: "+saga.Name, http.StatusNotFound)
				return
			}
			if env != "" {
				var envIndex int
				var foundEnv bool
				lg.Info("environment requested", zap.String("environment", env))
				// loop over the environments in the previously found scope looking for the query parmas value
				for e := range saga.Scopes[scopeIndex].Environments {
					if saga.Scopes[scopeIndex].Environments[e].Name == env {
						envIndex = e
						foundEnv = true
						break
					}
				}
				// return an error if requested env wasn't found.
				if !foundEnv {
					lg.Info("environment requested was not found", zap.String("environment", env))
					http.Error(w, "requested environment: "+env+" was not found in scope: "+scope+" in requested saga: "+saga.Name, http.StatusNotFound)
					return
				}
				// return the environent data since both scope and env were requested
				w.Header().Set("Content-Type", "application/json")
				json.NewEncoder(w).Encode(saga.Scopes[scopeIndex].Environments[envIndex].Data)
				return
			}

			// return only the requested scope data with all envs
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(saga.Scopes[scopeIndex])
			return
		}

		updated := qp.Get("updated")
		lg.Debug("value of updated", zap.String("updated", updated))
		if updated == "true" {
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(saga.Metadata.Updated)
			return
		}

		// return the entire saga since no scopes or envs were requested
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(saga)
		return
	}
}

// updateSaga func takes the adding service and http stuff and returns a func of http stuff and calls the service UpdateConfing, which takes the new Tree
func updateSaga(s sagas.Service, lg *zap.Logger) func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

		decoder := json.NewDecoder(r.Body)
		var sg sagas.Saga

		err := decoder.Decode(&sg)
		if err != nil {
			lg.Error("unable to decode request body", zap.String("url", r.Host+r.RequestURI))
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		n := p.ByName("sagaName")

		err = s.UpdateSaga(n, sg, lg)
		if err == sagas.ErrNotFound {
			http.Error(w, "the Saga you requested to update does not exist.", http.StatusNotFound)
			return
		}
		if err != nil {
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}

		if sg.Name != n {

			lg.Info("updated saga name changed", zap.String("new name", sg.Name))

			lg.Debug("debugging value of struct name", zap.String("new name", sg.Name))
			lg.Debug("debugging value of request name", zap.String("old name", n))
			lg.Debug("debugging value of request uri", zap.String("request uri", r.RequestURI))

			rduri := strings.Replace(string(r.RequestURI), n, sg.Name, -1)
			lg.Info("redirecting to new saga url", zap.String("new url", rduri))
			http.Redirect(w, r, "http://"+r.Host+rduri, http.StatusFound)
			return
		}

		lg.Info("redirecting to updated saga", zap.String("name", n), zap.String("url", "http://"+r.Host+r.RequestURI))
		http.Redirect(w, r, "http://"+r.Host+r.RequestURI, http.StatusFound)
		return
	}
}

// deleteSaga parses the router params and passes the string to the package to delete Saga by name
func deleteSaga(sg sagas.Service, lg *zap.Logger) func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

		sn := p.ByName("sagaName")

		err := sg.DeleteSaga(sn, lg)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}
		// rduri := strings.Replace(r.RequestURI, sn, "", -1)
		// log.Printf("[DEBUG] redirect uri after deletion is: %v", rduri)
		lg.Info("sucessfully deleted saga", zap.String("name", sn))
		//TODO: dynamically build the redirect uri. something is getting mangled with the above strings replace
		http.Redirect(w, r, "http://"+r.Host+"/api/rest/saga", http.StatusSeeOther)
	}
}
