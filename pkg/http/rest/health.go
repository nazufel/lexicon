package rest

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/ryanthebossross/lexicon/pkg/health"
	"go.uber.org/zap"
)

// healthz is the handler called for the Kuberenetes liveness probe functionality
func healthz(h health.Service, lg *zap.Logger) func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		err := h.Liveness(lg)
		if err != nil {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusInternalServerError) // 500
		}

		lg.Debug("liveness probe successful", zap.String("url", r.Host+r.RequestURI))
		w.Header().Set("Content-Type", "application/json")
	}
}
