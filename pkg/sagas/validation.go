package sagas

// Validation holds all the functions for validating the json requests coming into the server for Sagas. There are certain business rules that need to be inforced on model structure

var err error

// validations bootstraps the rest of the file
func validations(s Saga) error {

	// check for required fields takes a Saga and returns an error
	err := checkForRequiredFields(s)
	if err != nil {
		return err
	}

	err = checkForScopes(s)
	if err != nil {
		return err
	}

	return nil
}

// checkForRequiredfields takes a Saga, checks if all required fields are defined in a Saga, and returns an error
func checkForRequiredFields(s Saga) error {

	// check if Name is defined
	if len(s.Name) == 0 {
		return ErrUndefinedName
	}

	// check if Company is defined
	if len(s.Company) == 0 {
		return ErrUndefinedCompany
	}

	// return nil
	return nil
}

func checkForScopes(s Saga) error {

	if len(s.Scopes) == 0 {
		return ErrScopesUndefined
	}
	return nil
}
