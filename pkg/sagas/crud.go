// lexicon/pkg/sagas/crud.go

// file contains the use case methods for CRUD opertions on Saga objects

package sagas

import (
	"errors"
	"log"

	"go.uber.org/zap"
)

// ErrNotUnique Saga is not unqiue
var ErrNotUnique = errors.New("Saga is not unqiue")

// CreateSaga func is a method of the adding service that takes a Saga and calls the Create method of the Repository Service
func (s *service) CreateSaga(sg Saga, lg *zap.Logger) error {

	// run the received Saga model through validations, return validated model and an error
	err := validations(sg)
	if err != nil {
		return err
	}

	//TODO: go back and make sure dupes don't exist
	// sn := sg.Name
	// if sg.Unique {
	// 	sg.Name = ""
	// 	m, err := paramBuild(sg)
	// 	if err != nil {
	// 		return sg, err
	// 	}

	// 	var sgs []Saga
	// 	sgs, err = s.r.GetSagas(sg.Company, m)
	// 	if len(sgs) != 0 {
	// 		log.Printf("[INFO] query params for unique value are: %v", m)
	// 		return sg, ErrNotUnique
	// 	}
	// }

	// sg.Name = sn

	// pass the Saga to the repository for saving and return the error
	err = s.r.CreateSaga(sg, lg)
	if err != nil {
		log.Println(err)
	}

	return nil
}

// GetSaga takes the name of a Saga, calls the repository and passes in the name, then returns the found Saga, if any, and an error
func (s *service) GetSaga(n string, lg *zap.Logger) (Saga, error) {

	sg, err := s.r.GetSaga(n, lg)
	if err != nil {
		return sg, err
	}

	// return the found Saga and error
	return sg, nil
}

// GetSagas func takes a company and query parameters, passes them to the repository, and returns the found Sagas and an error
func (s *service) GetSagas(qp map[string][]string, lg *zap.Logger) ([]Saga, error) {

	var sgs []Saga

	// get all Sagas based on search params and return Sagas and an eror
	sgs, err := s.r.GetSagas(qp, lg)
	if err != nil {
		return sgs, err
	}

	// return Sagas and an error
	return sgs, nil
}

// TODO: check for unspecified fields and add null
// UpdateSaga func that takes a Saga and calls the UpdateSaga method of the Repository service
func (s *service) UpdateSaga(n string, sg Saga, lg *zap.Logger) error {

	// // check if the Type field is defined in the struct with expected values, if not, return an error
	// c, err := SagaValidations(c)
	// if err != nil {
	// 	return c, err
	// }

	// // findOneAndUpdate creates a document if document to be updated doesn't exist. Thus, searching for the doc first.
	// _, err = s.r.Get(com, cn)
	// if err == ErrNotFound {
	// 	log.Printf("[WARN] Saga not found: %v", cn)
	// 	return c, ErrNotFound
	// }

	// sn := c.Meta.Name
	// if c.Meta.Unique {
	// 	c.Meta.Name = ""
	// 	m, err := paramBuild(c)
	// 	if err != nil {
	// 		return c, err
	// 	}

	// 	var cs Sagas
	// 	cs, err = s.r.GetAll(c.Meta.Company, m)
	// 	if cs.Count != 0 {
	// 		log.Printf("[INFO] Saga set to unique, but found: %v identical Sagas.", cs.Count)
	// 		return c, ErrNotUnique
	// 	}
	// }

	err := s.r.UpdateSaga(n, sg, lg)
	if err == ErrNotFound {
		lg.Info("saga not found", zap.String("name", n))
		return err
	}
	if err != nil {
		lg.Error("saga update failed", zap.String("name", n), zap.Error(err))
		return err
	}
	if n != sg.Name {
		lg.Info("updated saga to new name", zap.String("old name", n), zap.String("new name", sg.Name))
		return nil
	}

	lg.Info("updated Saga", zap.String("name", sg.Name))

	return nil
}

// DeleteSaga func takes name string and calls the delete repository
func (s *service) DeleteSaga(sg string, lg *zap.Logger) error {
	err := s.r.DeleteSaga(sg, lg)
	if err != nil {
		return err
	}
	return nil
}
