package sagas

import (
	"testing"
)

// TestCheckRequiredFields is a table test that passes a part of a Saga to checkForRequiredFields and validates the returned error is expected
func TestCheckRequiredFields(t *testing.T) {
	tests := []struct {
		name  string
		input Saga
		want  error
	}{
		// Test cases for table test
		{name: "Test correctly defined required fields", input: Saga{Name: "config", Company: "Hogwarts"}, want: nil},
		{name: "Test undefined required field: name", input: Saga{Name: "", Company: "Hogwarts"}, want: ErrUndefinedName},
		{name: "Test undefined required field: type", input: Saga{Name: "config", Company: "Hogwarts"}, want: ErrUndefinedType},
		{name: "Test undefined required field: company", input: Saga{Name: "config", Company: ""}, want: ErrUndefinedCompany},
	}

	// loop over the test cases and check success criteria
	for _, tc := range tests {
		err := checkForRequiredFields(tc.input)
		if err != tc.want {
			t.Errorf("Check Field Required Fields test failed. Test name: %v. Expected: %v, got: %v", tc.name, tc.want, err)
		}
	}
}

// // TestValidateRequiredFields is a table test that passes a part of a Saga to validateRequiredFields and validates the returned error is expected
// func TestValidateRequiredFields(t *testing.T) {
// 	tests := []struct {
// 		name  string
// 		input Saga
// 		want  error
// 	}{
// 		{name: "Test acceptable required field: type", input: Saga{Name: "config", Company: "Hogwarts"}, want: nil},
// 		{name: "Test unacceptable required field: type", input: Saga{Name: "config", Company: "Hogwarts"}, want: ErrUnacceptableType},
// 	}

// 	// loop over the test cases and check success criteria
// 	for _, tc := range tests {
// 		err := validateRequiredFields(tc.input)
// 		if err != tc.want {
// 			t.Errorf("Test name: %v. Check Type Field test failed. Expected: %v, got: %v", tc.name, tc.want, err)
// 		}
// 	}
// }

// TestCheckForScopes is a table test that passes a part of a Saga to checkForScopes and validates that there is at least one scope defined
func TestCheckForScopes(t *testing.T) {
	tests := []struct {
		name  string
		input Saga
		want  error
	}{
		{name: "Test Saga with one scope defined", input: Saga{Name: "saga 1", Scopes: []Scope{{Name: "us-east-1"}}}, want: nil},
		{name: "Test Saga with multiple scopes defined", input: Saga{Name: "saga 1", Scopes: []Scope{{Name: "us-east-1"}, {Name: "us-west-1"}, {Name: "us-west-2"}}}, want: nil},
		{name: "Test Saga without a scope defined", input: Saga{Name: "saga 2", Scopes: []Scope{}}, want: ErrScopesUndefined},
	}

	// loop over the test cases and check success criteria
	for _, tc := range tests {
		err := checkForScopes(tc.input)
		if err != tc.want {
			t.Errorf("Check for Scopes failed. Test name: %v. Expected: %v, got: %v", tc.name, tc.want, err)
		}
	}
}
