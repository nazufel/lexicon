// pkg/sagas/models.go

package sagas

import "time"

// Saga struct holds information about Sagas
type Saga struct {
	Name                string      `json:"name"`
	Application         string      `json:"application"`
	Metadata            Metadata    `json:"metadata"`
	Company             string      `json:"company"`
	KuberntesConfigType string      `json:"kubernetesConfigType"`
	Scopes              []Scope     `json:"scopes"`
	Permissions         Permissions `json:"permissions"`
}

// Metadata struct holds information about Metadata
type Metadata struct {
	Description string    `json:"description"`
	Author      string    `json:"author"`
	Owner       string    `json:"owner"`
	Tags        []string  `json:"tags"`
	Created     time.Time `json:"created"`
	Updated     time.Time `json:"updated"`
}

// Scope struct holds information about scopes
type Scope struct {
	Name         string        `json:"name"`
	Environments []Environment `json:"environments"`
	Namespace    []Namespace   `json:"namespace"`
}

// Environment holds information about environments
type Environment struct {
	Name string `json:"name"`
	Data Data   `json:"data"`
}

// Namespace holds information for the namespace
type Namespace struct {
	Name string `json:"Name"`
	Data Data   `json:"data"`
}

// Data struct holds information about Data
type Data struct {
	Value    string `json:"value,omitempty"`
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

// Permissions struct holds informaton about Permissions
type Permissions struct {
	Read   []string `json:"read"`
	Write  []string `json:"write"`
	Delete []string `json:"delete"`
}
