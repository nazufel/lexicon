package sagas

// The service file defines the Service and repsoitory interfaces, allowing specific functions and models of the package to be available to other packes

import (
	"errors"

	"go.uber.org/zap"
)

// ErrDuplicate fig alredy exists
var ErrDuplicate = errors.New("saga name already exists")

// ErrNotFound not found error when searching for a saga
var ErrNotFound = errors.New("saga name could not be found")

// ErrUndefinedName of http request is required and undefined
var ErrUndefinedName = errors.New("name is a required field is undefined")

// ErrUndefinedType requires Type to be defined
var ErrUndefinedType = errors.New("type ia a required field is undefined")

// ErrUndefinedSubType requires Type to be defined
var ErrUndefinedSubType = errors.New("subtype ia a required field is undefined")

// ErrUndefinedScopes requires Type to be defined
var ErrUndefinedScopes = errors.New("no 'Scopes' are defined")

// ErrUndefinedCompany requires Type to be defined
var ErrUndefinedCompany = errors.New("company ia a required field is undefined")

// ErrUndefinedApplication requires Type to be defined
var ErrUndefinedApplication = errors.New("application ia a required field is undefined")

// ErrUndefinedEnvironments requires Type to be defined
var ErrUndefinedEnvironments = errors.New("environments is a required field is undefined")

// ErrUnacceptableType of http request is defined, but improperly formatted.
var ErrUnacceptableType = errors.New("unacceptable 'Type' defined")

// ErrScopesUndefined is an error if no scope is defined
var ErrScopesUndefined = errors.New("undefined 'Scope'")

// Service interface makes contained confings methods available outside of the package
type Service interface {
	CreateSaga(Saga, *zap.Logger) error
	GetSaga(string, *zap.Logger) (Saga, error)
	GetSagas(map[string][]string, *zap.Logger) ([]Saga, error)
	UpdateSaga(string, Saga, *zap.Logger) error
	DeleteSaga(string, *zap.Logger) error
}

// Repository interface makes contained methods of the Repository package(s) available to this package
type Repository interface {
	CreateSaga(Saga, *zap.Logger) error
	GetSaga(string, *zap.Logger) (Saga, error)
	GetSagas(map[string][]string, *zap.Logger) ([]Saga, error)
	UpdateSaga(string, Saga, *zap.Logger) error
	DeleteSaga(string, *zap.Logger) error
	DropSagasCollection(string, *zap.Logger) error
}

// struct holds references to other package Service interfaces and makes them available to this package
type service struct {
	r Repository
}

// NewService takes the repository interface and returns a pointer to the repository service
func NewService(r Repository) Service {
	return &service{r}
}
