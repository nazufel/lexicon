# Makefile

# Makefile to make life easier with some of these longer commands

# LICENSE GNU GPL v3.0

##--------------------------------------------------------------------------------##

build:
	$(clean_command):
	go build cmd/server/main.go

deps:
	$(clean_command):
	go get -d -v ./...

# build the multi-step container image
image:
	$(clean_command):
	docker build --no-cache -t lexikon/lexikon:latest .

push: 
	$(clean_command):
	docker push lexikon/lexikon:latest

# connect to mongo in the yelpcamp db using local auth.
mongo:
	$(clean_command):
	mongo mongodb://root:root@localhost:8081/lexicon?authSource=admin

# run the realize script
realize:
	$(clean_command):
	realize start

# build and run the binary
run:
	$(clean_command):
	rm -rf main
	go build cmd/server/main.go
	./main 

seed:
	$(clean_command):
	(cd ./cmd/seedData && go build && ./seedData sagas && rm -rf seedData)

# connect to mongo in the yelpcamp db using local auth.
test:
	$(clean_command):
	go test -v ./...

# start docker-compose
up:
	$(clean_command):
	docker-compose up -d

# useful commands that make can't handle
#	docker rmi $(docker images | grep "^<none>" | awk "{print $3}")

