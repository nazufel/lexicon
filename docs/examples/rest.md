# Lexicon Rest Examples

The current implimentation of Lexicon understands ReST. Below are examples of how to query the Lexicon ReST API. 

## Multi-Tenency

Lexicon uses multi-tenancy based on the company name to separate out configs. To access any C.R.U.D. operations, you must specify a company name.

## Setup

These examples assume you have a working server and database running and the server is listening on localhost:8000. All request examples are using curl, but the POST/PUT verbs have payloads taken out of Postman.

## Creating Configuration Items

Creating a configuration item takes a POST request to `/api/rest/<company-name>/configs` with the fields defined. 
```bash
curl -X POST \
  http://localhost:8000/api/rest/lexicon-industries/configs/ \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Type: application/x-www-form-urlencoded,text/plain' \
  -H 'Referer: http://localhost:8000/api/rest/lexicon-industries/configs' \
  -H 'cache-control: no-cache' \
  -d '{
    "name": "redis-connection-string",
    "unique": false,
    "type": "connection-string",
    "scope": "regions",
    "company": "lexicon-industries",
    "applications": [
        "lexicon",
        "lexicon-legacy",
        "a-whole-new-platform",
        "lexicon-2"
    ],
    "environments": [
        "dev",
        "qa",
        "stage",
        "prod"
    ],
    "regions": [
        "us-east-1"
    ],
    "data": {
        "connection-string": "redis1:6380,redis2:6380,redis3:6380,allowAdmin=true"
    },
    "description": "Redis multiplexer connection string.",
    "author": "ron-weasley",
    "owner": "ron-weasley",
    "tags": [
        "redis",
        "cache",
        "lexicon",
        "dotnet",
        "dev",
        "qa"
    ],
    "permissions": {
        "read": [
            "default",
            "admins",
            "developers",
            "ops",
            "devops",
            "it-managers"
        ],
        "write": [
            "admins",
            "developers",
            "ops",
            "devops",
            "it-managers"
        ],
        "delete": [
            "admins",
            "developers",
            "ops",
            "devops",
            "it-managers"
        ]
    }
}'
```

This will create a config named, *redis-connection-string* in the *lexicon-industries* company. Upon successful creation, the server will send a redirect to the URL where that configuration item can be viewed: `/api/rest/lexicon-industries/configs/redis-connection-string`.

### Unique
Coming Soon.

### Environment
Coming Soon.

## Reading Configuration Items

Lexicon provides two primary ways for gettings configuration items: 1. get one by name, 2. get all based on query parameters, or if no query parameters are defined then get all configs based on the company provided in the url path.

### Get One by Name

Reading a single configuration item by name is the primary way retrieve configurations. This can be done by sending a GET request with the dash separated name to: `/api/rest/<company-name>/configs/<configuration-name>`. To retrieve the above example configuraiton, use curl:
```bash
curl http://localhost:8000/api/rest/lexicon-industries/configs/redis-connection-string
```

This will return a json document with only the `data` field(s) defined:
```json
{
    "connection-string": "redis1:6380,redis2:6380,redis3:6380,allowAdmin=true"
}
```
Since the configuration item requested is a `connection-string`, only the type and data are returned. This is the reccomened way for machines to get configuration item values. If a deploy server is needs the Redis connection string for the application it's deploying, it doesn't need all the metadata associated with the configuration item. It just needs the connection string. 

There is a URL parameter to get all of the metadata associated with a configuration item: `describe=true`. This parameter will return everything Lexicon knows about that configuration item:
```
curl http://localhost:8000/api/rest/lexicon-industries/configs/redis-connection-string?describe=true
```

This will return the full configuration with some other information that was not explicitly defined at creation, such as created and updated date:time stamps.
```json
{
    "name": "redis-connection-string",
    "unique": false,
    "type": "connection-string",
    "scope": "regions",
    "company": "lexicon-industries",
    "applications": [
        "lexicon",
        "lexicon-legacy",
        "a-whole-new-platform",
        "lexicon-2"
    ],
    "environments": [
        "dev",
        "qa",
        "stage",
        "prod"
    ],
    "regions": [
        "us-east-1"
    ],
    "data": {
        "connection-string": "redis1:6380,redis2:6380,redis3:6380,allowAdmin=true"
    },
    "description": "Redis multiplexer connection string.",
    "author": "ron-weasley",
    "owner": "ron-weasley",
    "created": "2019-09-25T19:04:29.832Z",
    "updated": "2019-09-25T19:05:21.509Z",
    "tags": [
        "redis",
        "cache",
        "lexicon",
        "dotnet",
        "dev",
        "qa"
    ],
    "permissions": {
        "read": [
            "default",
            "admins",
            "developers",
            "ops",
            "devops",
            "it-managers"
        ],
        "write": [
            "admins",
            "developers",
            "ops",
            "devops",
            "it-managers"
        ],
        "delete": [
            "admins",
            "developers",
            "ops",
            "devops",
            "it-managers"
        ]
    }
}
```

### Get All Configuration Items

There are two ways to get all configuration items: 1. get everything for a company, and 2. get all based on url query parameters.

#### Get All for a Company

Getting all the configurations for a company requires specifying the company in the route: `/api/rest/<company-name>/configs/`. A curl example based on the above created configuration is below:

```
curl http://localhost:8000/api/rest/lexicon-industries/configs/
```

The returned response is a single json object with some meta data at the top level, then a list of all the configuration items within their own json objects and sub-objects. 
```json
{
    "status": "ok",
    "count": 2,
    "configs": [
        {
            "name": "redis-connection-string",
            "unique": false,
            "type": "connection-string",
            "scope": "regions",
            "company": "lexicon-industries",
            "applications": [
                "lexicon",
                "lexicon-legacy",
                "a-whole-new-platform",
                "lexicon-2"
            ],
            "environments": [
                "dev",
                "qa",
                "stage",
                "prod"
            ],
            "regions": [
                "us-east-1"
            ],
            "data": {
                "connection-string": "redis1:6380,redis2:6380,redis3:6380,allowAdmin=true"
            },
            "description": "Redis multiplexer connection string.",
            "author": "ron-weasley",
            "owner": "ron-weasley",
            "created": "2019-09-26T14:11:45.267Z",
            "updated": "2019-09-26T14:11:45.267Z",
            "tags": [
                "redis",
                "cache",
                "lexicon",
                "dotnet",
                "dev",
                "qa"
            ],
            "permissions": {
                "read": [
                    "default",
                    "admins",
                    "developers",
                    "ops",
                    "devops",
                    "it-managers"
                ],
                "write": [
                    "admins",
                    "developers",
                    "ops",
                    "devops",
                    "it-managers"
                ],
                "delete": [
                    "admins",
                    "developers",
                    "ops",
                    "devops",
                    "it-managers"
                ]
            }
        },
        {
            "name": "mongodb-connection-string",
            "unique": false,
            "type": "connection-string",
            "scope": "environments",
            "company": "lexicon-industries",
            "applications": [
                "lexicon"
            ],
            "environments": [
                "dev"
            ],
            "regions": [
                "us-east-1"
            ],
            "data": {
                "connection-string": "lexicon-user:dkfnwOIUke899df@dbcluster01.prod01.lexicon.com"
            },
            "description": "Prod Redis multiplexer connection string.",
            "author": "severus-snape",
            "owner": "albus-dumbledore",
            "created": "2019-09-26T14:11:45.281Z",
            "updated": "2019-09-26T14:11:45.281Z",
            "tags": [
                "mongodb",
                "lexicon",
                "golang",
                "prod"
            ],
            "permissions": {
                "read": [
                    "default",
                    "admins",
                    "operations",
                    "it-managers"
                ],
                "write": [
                    "admins",
                    "operations",
                    "it-managers"
                ],
                "delete": [
                    "admins",
                    "operations",
                    "it-managers"
                ]
            }
        }
    ]
}
```

#### Get All for Query Parameters

Lexicon accepts URL query parameters. Use these parameters to search for anything of a certain criteria. If Lexicon finds more than one configuration item matching the search parameters, then it will return a json object with an array of configuration items like above. 

```
curl http://localhost:8000/api/rest/lexicon-industries/configs/?environments=dev&regsions=us-east-1
```

This query will ask the server for all configs associated with the "dev" environment and in the "us-east-1" region.

```json
{
    "status": "ok",
    "count": 2,
    "configs": [
        {
            "name": "redis-connection-string",
            "unique": false,
            "type": "connection-string",
            "scope": "regions",
            "company": "lexicon-industries",
            "applications": [
                "lexicon",
                "lexicon-legacy",
                "a-whole-new-platform",
                "lexicon-2"
            ],
            "environments": [
                "dev",
                "qa",
                "stage",
                "prod"
            ],
            "regions": [
                "us-east-1"
            ],
            "data": {
                "connection-string": "redis1:6380,redis2:6380,redis3:6380,allowAdmin=true"
            },
            "description": "Redis multiplexer connection string.",
            "author": "ron-weasley",
            "owner": "ron-weasley",
            "created": "2019-09-26T14:11:45.267Z",
            "updated": "2019-09-26T14:11:45.267Z",
            "tags": [
                "redis",
                "cache",
                "lexicon",
                "dotnet",
                "dev",
                "qa"
            ],
            "permissions": {
                "read": [
                    "default",
                    "admins",
                    "developers",
                    "ops",
                    "devops",
                    "it-managers"
                ],
                "write": [
                    "admins",
                    "developers",
                    "ops",
                    "devops",
                    "it-managers"
                ],
                "delete": [
                    "admins",
                    "developers",
                    "ops",
                    "devops",
                    "it-managers"
                ]
            }
        },
        {
            "name": "mongodb-connection-string",
            "unique": false,
            "type": "connection-string",
            "scope": "environments",
            "company": "lexicon-industries",
            "applications": [
                "lexicon"
            ],
            "environments": [
                "dev"
            ],
            "regions": [
                "us-east-1"
            ],
            "data": {
                "connection-string": "lexicon-user:dkfnwOIUke899df@dbcluster01.prod01.lexicon.com"
            },
            "description": "Prod Redis multiplexer connection string.",
            "author": "severus-snape",
            "owner": "albus-dumbledore",
            "created": "2019-09-26T14:11:45.281Z",
            "updated": "2019-09-26T14:11:45.281Z",
            "tags": [
                "mongodb",
                "lexicon",
                "golang",
                "prod"
            ],
            "permissions": {
                "read": [
                    "default",
                    "admins",
                    "operations",
                    "it-managers"
                ],
                "write": [
                    "admins",
                    "operations",
                    "it-managers"
                ],
                "delete": [
                    "admins",
                    "operations",
                    "it-managers"
                ]
            }
        }
    ]
}
```
Two configuration items are returned: `redis-connection-string` and `mongodb-connection-string`. Since there are two items, the server sends back all of the meta data information.

However, if only one configuration is found, it will return just the data field(s) of that configuration, unless the query parameter of `describe=true` is defined, like above.

```
curl http://localhost:8000/api/rest/lexicon-industries/configs?environments=dev&regions=us-east-1&scope=regions
```
This query returns back one config. Since there's only one, the server responds with just the data:

```json
{
    "connection-string": "redis2:6380,redis3:6380,allowAdmin=true"
}
```

If `describe=true` is passed, then the query that only returns a single config data will return all of the data about it.

```
curl http://localhost:8000/api/rest/lexicon-industries/configs?environments=dev&regions=us-east-1&scope=regions&desribe=true
```

## Updating Configuration Items

Updating a configuration item requires a PUT http verb and the entire configuration item. Following ReST conventions, using PUT to the route of the configuration item's name, `/api/rest/<company-name>/configs/<config-name>`, updates the entire configuration item. Be sure you have all of the fields filled out when updating. Any empty fields will be empty in the final configuration.

```bash
curl -X POST \
  http://localhost:8000/api/rest/lexicon-industries/configs/ \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Type: application/x-www-form-urlencoded,text/plain' \
  -H 'Referer: http://localhost:8000/api/rest/lexicon-industries/configs' \
  -H 'cache-control: no-cache' \
  -d '{
    "name": "redis-connection-string",
    "unique": false,
    "type": "connection-string",
    "scope": "regions",
    "company": "lexicon-industries",
    "applications": [
        "lexicon",
        "lexicon-legacy",
        "a-whole-new-platform",
        "lexicon-2"
    ],
    "environments": [
        "dev",
        "qa",
        "stage",
        "prod"
    ],
    "regions": [
        "us-east-1"
    ],
    "data": {
        "connection-string": "redis4:6380,redis5:6380,redis6:6380,allowAdmin=true"
    },
    "description": "Redis multiplexer connection string with updated redis servers.",
    "author": "ron-weasley",
    "owner": "ron-weasley",
    "tags": [
        "redis",
        "cache",
        "lexicon",
        "dotnet",
        "dev",
        "qa"
    ],
    "permissions": {
        "read": [
            "default",
            "admins",
            "developers",
            "ops",
            "devops",
            "it-managers"
        ],
        "write": [
            "admins",
            "developers",
            "ops",
            "devops",
            "it-managers"
        ],
        "delete": [
            "admins",
            "developers",
            "ops",
            "devops",
            "it-managers"
        ]
    }
}'

```
This PUT request changes the server names in the data field and updates the description. Upon acceptance, the server will repsond with a redirect to the location of the updated configuration. The redirect will also work if the config is renamed and will redirect to the new location.

## Deleting Configuration Item

Deleting is the simplest operation of all. The server expects a DELETE request to the configuration item's name endpoint, `/api/rest/<company-name>/configs/<config-name>` and the server will delete the configuration. To delete the example configuration:
```bash
curl -X DELETE \
  http://localhost:8000/api/rest/lexicon-industries/configs/redis-connection-string \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Referer: http://localhost:8000/api/rest/lexicon-industries/configs/redis-connection-string' \
  -H 'User-Agent: PostmanRuntime/7.17.1' \
  -H 'cache-control: no-cache'
```
Upon successful deletion, the server will redirect to the Get All endpoint as described above. 