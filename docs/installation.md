# Installing

Lexicon was written in Golang and uses version [1.13](https://golang.org/dl/). Make sure your system has Golang installed. To run follow these instructions:

Clone the repositry

```
git clone https://gitlab.com/ryanthebossross/lexicon.git
```

Change into the directory and install the dependencies from the root of the project

```
go get -d -v ./...
```

## Running the Application

### Environment Variables

 Lexicon is [12-Factor](https://12factor.net/) compliant. Following the factors, Lexicon uses environment variables for configuration items. Here is a table of environment variables used by Lexicon and their default values, if any. This table will get updated as new variables are added. Some variables may not be in their final format. This is will be part of MVP for Lexicon v1.0 to finalize variables. Lexicon will look to the environment for values before providing defaults, which usually should only work in developement. It is reccomended to override these values in higher environments with the `env` spec for Kubernetes.

|Name | Required | Use | 
| --- | --- | --- |
| dbHostENV | Yes | Define the location of the DB host/cluster/LB. This can be a DNS name, IP, or Kubernetes service name |
| dbNameENV | Yes | Define the database name |
| dbPasswordENV | Yes | Define a password for the database user |
| dbPortENV | Yes | Define the port the DB is listneing on |
| dbUsernameENV | Yes | Define user to access the database |
| GCMKeyENV | Yes | A GCM Key is needed to decode the encryption of the Config.Data struct. The key needs to be a []byte 32 bytes long. The env will take a string and convert to bytes to feed into the encryption/decyrption functions. Note: these values are from golang's examples. Don't use these in production! | 
| GCMNonceENV | Yes | A nonce of []byte with a length of 12 is needed for gcm encryption/decryption. An os.Getenv is set up to look for a string that will convert to []byte. Default value is a []byte. Note: these values are from golang's examples. Don't use these in production! |
| dropColENV | No | Define if the collection drop function can be called. This is only for local dev and is not meant to be defined in higher environments | 
| servicePortENV | Yes | Port the webserver is listening on | 
 
 #### Building and Running the Binary on the Machine

 Run the main function in the cmd directory to start the server

```
go run cmd/lexicon-server/main.go
```

Be sure to read the Depenencies section for more information on depencies. Lexicon expects to have a database before it starts the server. Before starting the server, make sure the database is running and accessable. 

#### Makefile
A Makefile has been provided to shorten up development commands. Here are the acceptable make commands:
* image - builds a Docker image using the Dockerfile at the root of the repo tagged `development`
* mongo - uses the MongoDB JS client to connect to the local MongoDB
* run - builds and runs and the Lexicon server binary
* seed - runs the seed program to seed the local DB with development data, requires the database to be up
* test - runs all of the unit tests 
* up - stands up docker-compose of the Lexicon container and MongoDB