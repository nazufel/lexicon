# Logging

This page discusses how logging is implimented in Lexicon and how to change the logging levels.

## Logging Library

Lexicon uses [Zap](https://github.com/uber-go/zap) by Uber for structured logging. The structued logs output as json in the console:

```json
{"level":"info","ts":1581946545.4564438,"caller":"lexicon-server/main.go:62","msg":"webserver is up and running at port","port":"3000"}
```

Structured logs in json are helpful becuase they will be easily parsed by logging aggregation systems or just looking at the commandline.

## Structuring the Structured Logs

## Log Levels

Logging in Lexicon follows the logging levels outlined in this [Stackify](https://stackify.com/logging-levels-101/) article. Please read the article before working with logging in Lexicon.

By default, ```debug``` logging is turned off. It is enabled with an environment variable of ```logLevel=debug``` that the app looks for on start up. Debug level logging should not be used in production, but is meant to help developers, operators, and testers in lower environments. 

## Standard Streams

Logs from Lexicon are written to either [STDOUT](https://en.wikipedia.org/wiki/Standard_streams#Standard_output_(stdout)) or [STDERR](https://en.wikipedia.org/wiki/Standard_streams#Standard_error_(stderr)) of the local host the webserver is running on. If logs need to be written to a file or exported to an aggregator, then the use of an [Adapter Container](https://kubernetes.io/blog/2015/06/the-distributed-system-toolkit-patterns/) should be implimented in the Pod at runtime.


## TODOs
* Change the timestamp format from Unix time to RFC 2822