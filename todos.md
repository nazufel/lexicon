# TODO's
* No Global state or default variables
* Embeded references, like Kubernetes looking up configmaps for env variables. - Not sure I want to do this anymore. The initial vision was like how my company handles it, but I'm liking that implimentation less and less. However, I think Parent/Child could work.
    * Parent/Child relationships
* Permissions system: read, write, delete groups and Company
* Testing Suite
* Each company is a different collection in the configs DB - maybe, need a better model for multi-tenency.
* Redirects
* Deleting a config goes to a temporary collection and is truly deleted after 30 days (default) or after a configured amount of time.
* Encryption of the data object of the configs
* Kubernetes health checks
* Proper logging middleware to follow [this](https://www.scalyr.com/blog/the-10-commandments-of-logging/) format
* Service Accounts (i.e. programatic clients: cli, language libraries, CI/CD plugin accounts etc.) and User Accounts, real humans logging in
* Text index searching
* Impliment validation to check for unique configuration values and respond with similar configurations. 
* Configurations similarities can only be overriden by Type administrators.
* Use a cache for frequent reads
* Use projection to return only data object or whole config when `describe` is used
* check and add null to unspecified fields in update
* permissions per tree along with perms for the whole root
* toggleable data encryption
* break out validations and strings convert into a separate package
* single command install into a k8s cluster with happy defaults
* redis caching, optional/configuratble
* optional/configurable dbs: document, sql
* cli: `lexctl`
* graphql api
* grpc api
* grpc between cli and server
* separate logging manager using goroutines
* context
* mutex vars on start up
* undefined fields get a null
* add funtionality to namespaces