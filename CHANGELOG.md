# Change Log 

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)

## [Unrealeased]

## Released

## v0.1 - 2020-01-18

### Added
- Renamed treeitems to Sagas by [ryanthebossross](https://gitlab.com/ryanthebossross/)
- Create, Read, Update, Delete of Configuration objects 
- Type field of Config model valdiation with unit tests
- Seed DB with development data
- Default response for single config look up is `Config.Data`
- Use URL query param `describe=true` to respond with full `Config`
- Validate and convert `Config.Name` to be lowercase and any spaces joined with a dash (-). Unit tests included.
- Settled on `Config.Type`s: "Connection String", "Password", "Hostnames", and "Key Value"
- Settled on `Config.Scope`s: "Gobal", "Application", "Environment", and "Hosts"
- New field of `Region`
- Can use query params to search for top level config fields
- Removed `Meta` struct from `Config`. All fields are now part of `Config`. Only nested structs in Config are `Data` and `Permissions`. This now allows those fields to be searchable.
- Changed function names to be more concise and in line with Mat Ryer.
- Added Encryption of config Data fields
- Changed a bug in the update functionality
- Added Images build and works in Kubernetes
- Added liveness probe to deployments
- Added unique property and enforcement - commit 05e6a69e9155b8dd6c1bfb2f1ae07e289b228fd9

### Bug Fixes
- Bug001: Get all configs no longer populates all data objects - Fixed - commit 4bc0ba7a6207b9c2fb7cf32650262cafd630a6ae
