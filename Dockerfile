# lexicon/Dockerfile

# Multi-Stage Dockerfile to build a scratch Docker Image of the app 

##--------------------------------------------------------------------------------##

##################
### test stage ###
##################

# pull in the golang 1.14 alpine image 
FROM golang:1.14

# set the working directory
WORKDIR /go/src/lexicon

# add project files for the app
ADD ./cmd/server/ .

# install app dependencies
RUN go get -d -v ./...

# run unit tests
RUN go test -v ./...

#####################
### builder stage ###
#####################

# # pull in the builder base image 
FROM golang:1.14 AS builder

# # set the working directory
WORKDIR /go/src/lexicon

# TODO: add only source files without test files
# add project files for the app
ADD ./cmd/server/ .

# install app dependencies
RUN go get -d -v ./...

# build the app with compiler flags for os, arch, and to remove debug information for smaller images
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o lexicon

###################     
### final stage ###
###################

## create minimal scratch image
FROM scratch

## set the working directory
WORKDIR /go/bin/lexicon

## copy the combiled binary to the smaller image
COPY --from=builder /go/src/lexicon/lexicon .

## start the binary when the container starts
ENTRYPOINT ["./lexicon"]
